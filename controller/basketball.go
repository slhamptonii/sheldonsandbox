package controller

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"log"
	"net/http"
	"sheldonsandbox/model"
	"strconv"
)

type RatePlayersResponse struct {
	Team    []model.Player `yaml:"team"`
	Players []model.Player `yaml:"players"`
}

func RatePlayers(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
		return
	}

	var config model.ScoreConfig
	if len(r.Form) > 0 {
		config, err = buildScoreConfigFromForm(r)
		if err != nil {
			log.Println(err)
			redirectWithError(http.StatusInternalServerError, w)
			return
		}
	}

	session, err := store.Get(r, sessionName)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
		return
	}

	//TODO - make service request to rate players and get back []model.Player
	res, err := basketballRequester.Post("/ratings", map[string]string{"Content-Type": "application/yaml"}, config)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
		return
	}

	var ratingsRes RatePlayersResponse
	err = yaml.NewDecoder(res.Body).Decode(&ratingsRes)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
		return
	}

	var clientData ClientData
	clientData.Team, clientData.Players, clientData.ScoreConfig = ratingsRes.Team, ratingsRes.Players, config

	err = session.Save(r, w)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
		return
	}

	err = templates.Lookup("projs.html").Execute(w, clientData)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
		return
	}
}

func buildScoreConfigFromForm(r *http.Request) (model.ScoreConfig, error) {
	rangeVals, err := extractFieldsFromForm("Range", r)
	if err != nil {
		return model.ScoreConfig{}, nil
	}
	numVals, err := extractFieldsFromForm("Number", r)
	if err != nil {
		return model.ScoreConfig{}, nil
	}
	configRange := model.ScoreConfig{
		Min: rangeVals["min"],
		Fgm: rangeVals["fgm"],
		Fga: rangeVals["fga"],
		Fgp: rangeVals["fgp"],
		Ftm: rangeVals["ftm"],
		Fta: rangeVals["fta"],
		Ftp: rangeVals["ftp"],
		Tpm: rangeVals["tpm"],
		Tpa: rangeVals["tpa"],
		Tpp: rangeVals["tpp"],
		Reb: rangeVals["reb"],
		Ass: rangeVals["ass"],
		Stl: rangeVals["stl"],
		Blk: rangeVals["blk"],
		Tvs: rangeVals["tvs"],
		Dds: rangeVals["dds"],
		Pts: rangeVals["pts"],
	}

	configNumber := model.ScoreConfig{
		Min: numVals["min"],
		Fgm: numVals["fgm"],
		Fga: numVals["fga"],
		Fgp: numVals["fgp"],
		Ftm: numVals["ftm"],
		Fta: numVals["fta"],
		Ftp: numVals["ftp"],
		Tpm: numVals["tpm"],
		Tpa: numVals["tpa"],
		Tpp: numVals["tpp"],
		Reb: numVals["reb"],
		Ass: numVals["ass"],
		Stl: numVals["stl"],
		Blk: numVals["blk"],
		Tvs: numVals["tvs"],
		Dds: numVals["dds"],
		Pts: numVals["pts"],
	}

	if ok, err := configRange.Equals(&configNumber); !ok {
		return model.ScoreConfig{}, err
	} else {
		return configRange, nil
	}
}

func extractFieldsFromForm(s string, r *http.Request) (map[string]float32, error) {
	rangeVals := make(map[string]float32, 0)

	for _, field := range model.ScoreConfigFields {
		fullField := field + s
		if f, err := strconv.ParseFloat(r.FormValue(fullField), 32); err == nil {
			rangeVals[field] = float32(f)
		} else {
			log.Println(fmt.Sprintf("error extracting %s", fullField))
			log.Println(err.Error())
			return make(map[string]float32), err
		}

	}

	return rangeVals, nil
}

package controller

import (
	"github.com/gorilla/sessions"
	"github.com/robfig/cron/v3"
	"log"
	"net/http"
	"sheldonsandbox/generate"
)

//session store
var store *sessions.CookieStore
var sessionName = "sheldonsandboxsession"

//initializes application session store
func LoadSession() bool {
	authKey := generate.String(32)
	encKey := generate.String(32)

	//create sessions store
	store = sessions.NewCookieStore([]byte(authKey), []byte(encKey))

	//setup key rotation
	c := cron.New()
	_, err := c.AddFunc("30 * * * *", func() { //every hour on the half hour
		newAuthKey, newEncKey := generate.String(32), generate.String(32)
		store = sessions.NewCookieStore(
			[]byte(newAuthKey),
			[]byte(newEncKey),
			[]byte(authKey),
			[]byte(encKey),
		)
		authKey, encKey = newAuthKey, newEncKey
		newAuthKey, newEncKey = "", ""
	})
	if err != nil {
		log.Println(err)
		return false
	}

	//allow serialization of core models
	//gob.Register(&ClientData{})

	log.Println("session store loaded successfully")
	return true
}

//sessions middleware
func Sessions(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//get a session. Get() always returns a session, even if empty.
		session, _ := store.Get(r, sessionName)

		//configure session options
		session.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   86400 * 7, //one week
			HttpOnly: true,
		}

		//save session
		err := session.Save(r, w)
		if err != nil {
			log.Println("error saving session")
			log.Println(err)
			redirectWithError(http.StatusInternalServerError, w)
			return
		}

		h.ServeHTTP(w, r)
	})
}

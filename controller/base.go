package controller

import (
	"log"
	"net/http"
	"sheldonsandbox/model"
)

func Home(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("home.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

func Like(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("like.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

func NotLike(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("!like.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

func Imgs(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("imgs.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

func Projs(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("projs.html").Execute(w, ClientData{Players:[]model.Player{}, ScoreConfig:model.ScoreConfig{
		Min: 1,
		Fgm: 1,
		Fga: 1,
		Fgp: 1,
		Ftm: 1,
		Fta: 1,
		Ftp: 1,
		Tpm: 1,
		Tpa: 1,
		Tpp: 1,
		Reb: 1,
		Ass: 1,
		Stl: 1,
		Blk: 1,
		Tvs: 1,
		Dds: 1,
		Pts: 1,
	}})
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

func Ppl(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("ppl.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

func About(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("about.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
		redirectWithError(http.StatusInternalServerError, w)
	}
}

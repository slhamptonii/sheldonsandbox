package controller

import "net/http"

func redirectWithError(code int, w http.ResponseWriter) {
	//TODO: give each error a better message
	switch code {
	case 400:
		_ = templates.Lookup("400.html").Execute(w, nil)
		break
	case 403:
		_ = templates.Lookup("login.html").Execute(w, nil)
		break
	case 404:
		_ = templates.Lookup("404.html").Execute(w, nil)
		break
	default:
		_ = templates.Lookup("500.html").Execute(w, nil)
	}
}

package controller

import (
	"html/template"
	"log"
	"os"
	"sheldonsandbox/model"
)

type ClientData struct {
	Errs        []string
	ScoreConfig model.ScoreConfig
	Players     []model.Player
	Team []model.Player
}

var functions template.FuncMap
var templates *template.Template

func LoadTemplates() bool {
	t := template.New("templates").Funcs(functions)
	log.Print("template functions loaded successfully")

	basePath := "templates"
	templateFolder, err := os.Open(basePath)
	if err != nil {
		log.Fatal(err)
	}
	defer templateFolder.Close()

	templatePathsRaw, _ := templateFolder.Readdir(-1)

	templatePaths := new([]string)
	for _, pathInfo := range templatePathsRaw {
		if !pathInfo.IsDir() {
			*templatePaths = append(*templatePaths, basePath+"/"+pathInfo.Name())
		}
	}

	templates = template.Must(t.ParseFiles(*templatePaths...))

	log.Print("templates loaded successfully")

	return true
}

func LoadFunctions() bool {
	functions = template.FuncMap{
		"inc": func(i int) int {
			return i + 1
		},
	}

	return true
}

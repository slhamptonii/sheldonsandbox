package controller

import (
	"log"
	"os"
	"sheldonsandbox/request"
)

var basketballRequester request.Requester

func LoadRequesters() bool {
	basketballUrl := os.Getenv("BASKETBALL_URL")
	basketballRequester = request.NewRequester(basketballUrl)
	log.Println("requesters loaded successfully")

	return true
}

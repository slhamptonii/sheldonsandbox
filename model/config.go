package model

import (
	"errors"
	"fmt"
	"time"
)

var ScoreConfigFields = []string{"min", "fgm", "fga", "fgp", "ftm", "fta", "ftp", "tpm", "tpa", "tpp", "reb", "ass", "stl", "blk", "tvs", "dds", "pts"}

type ScoreConfig struct {
	Min float32 `yaml:"min" validate:"min=0,max=10,required"`
	Fgm float32 `yaml:"fgm"  validate:"min=0,max=10,required"`
	Fga float32 `yaml:"fga"  validate:"min=0,max=10,required"`
	Fgp float32 `yaml:"fgp"  validate:"min=0,max=10,required"`
	Ftm float32 `yaml:"ftm"  validate:"min=0,max=10,required"`
	Fta float32 `yaml:"fta"  validate:"min=0,max=10,required"`
	Ftp float32 `yaml:"ftp"  validate:"min=0,max=10,required"`
	Tpm float32 `yaml:"tpm"  validate:"min=0,max=10,required"`
	Tpa float32 `yaml:"tpa"  validate:"min=0,max=10,required"`
	Tpp float32 `yaml:"tpp"  validate:"min=0,max=10,required"`
	Reb float32 `yaml:"reb"  validate:"min=0,max=10,required"`
	Ass float32 `yaml:"ass"  validate:"min=0,max=10,required"`
	Stl float32 `yaml:"stl"  validate:"min=0,max=10,required"`
	Blk float32 `yaml:"blk"  validate:"min=0,max=10,required"`
	Tvs float32 `yaml:"tvs"  validate:"min=0,max=10,required"`
	Dds float32 `yaml:"dds"  validate:"min=0,max=10,required"`
	Pts float32 `yaml:"pts"  validate:"min=0,max=10,required"`
}

func (config *ScoreConfig) Score(player *Player) {
	score := float32(0.0)
	numGames := float32(player.Gms)

	score += float32(player.Min) / numGames * config.Min
	score += player.Fgm / numGames * config.Fgm
	score += player.Fga / numGames * config.Fga
	score += player.Fgp * config.Fgp
	score += player.Ftm / numGames * config.Ftm
	score += player.Fta / numGames * config.Fta
	score += player.Ftp * config.Ftp
	score += player.Tpm / numGames * config.Ftm
	score += player.Tpa / numGames * config.Tpa
	score += player.Tpp * config.Tpp
	score += player.Reb * config.Reb
	score += player.Ass * config.Ass
	score += player.Stl * config.Stl
	score += player.Blk * config.Blk
	score += player.Tvs * config.Tvs
	score += player.Dds / numGames * config.Dds
	score += player.Pts / numGames * config.Pts

	player.Score = score
	player.UpdatedDateTime = time.Now()
}

func (conf1 *ScoreConfig) Equals(conf2 *ScoreConfig) (bool, error) {
	ok := true
	var err error
	errString := ""

	if conf1.Min != conf2.Min {
		ok = false
		errString += fmt.Sprintf("min::conf1:%v::conf2:%v::", conf1.Min, conf2.Min)
	}

	if conf1.Fgm != conf2.Fgm {
		ok = false
		errString += fmt.Sprintf("fgm::conf1:%v::conf2:%v::", conf1.Fgm, conf2.Fgm)
	}

	if conf1.Fga != conf2.Fga {
		ok = false
		errString += fmt.Sprintf("fga::conf1:%v::conf2:%v::", conf1.Fga, conf2.Fga)
	}

	if conf1.Fgp != conf2.Fgp {
		ok = false
		errString += fmt.Sprintf("fgp::conf1:%v::conf2:%v::", conf1.Fgp, conf2.Fgp)
	}

	if conf1.Ftm != conf2.Ftm {
		ok = false
		errString += fmt.Sprintf("ftm::conf1:%v::conf2:%v::", conf1.Ftm, conf2.Ftm)
	}

	if conf1.Fta != conf2.Fta {
		ok = false
		errString += fmt.Sprintf("fta::conf1:%v::conf2:%v::", conf1.Fta, conf2.Fta)
	}

	if conf1.Ftp != conf2.Ftp {
		ok = false
		errString += fmt.Sprintf("ftp::conf1:%v::conf2:%v::", conf1.Ftp, conf2.Ftp)
	}

	if conf1.Tpm != conf2.Tpm {
		ok = false
		errString += fmt.Sprintf("tpm::conf1:%v::conf2:%v::", conf1.Tpm, conf2.Tpm)
	}

	if conf1.Tpa != conf2.Tpa {
		ok = false
		errString += fmt.Sprintf("tpa::conf1:%v::conf2:%v::", conf1.Tpa, conf2.Tpa)
	}

	if conf1.Tpp != conf2.Tpp {
		ok = false
		errString += fmt.Sprintf("tpp::conf1:%v::conf2:%v::", conf1.Tpp, conf2.Tpp)
	}

	if conf1.Reb != conf2.Reb {
		ok = false
		errString += fmt.Sprintf("reb::conf1:%v::conf2:%v::", conf1.Reb, conf2.Reb)
	}

	if conf1.Ass != conf2.Ass {
		ok = false
		errString += fmt.Sprintf("ass::conf1:%v::conf2:%v::", conf1.Ass, conf2.Ass)
	}

	if conf1.Stl != conf2.Stl {
		ok = false
		errString += fmt.Sprintf("stl::conf1:%v::conf2:%v::", conf1.Stl, conf2.Stl)
	}

	if conf1.Blk != conf2.Blk {
		ok = false
		errString += fmt.Sprintf("blk::conf1:%v::conf2:%v::", conf1.Blk, conf2.Blk)
	}

	if conf1.Tvs != conf2.Tvs {
		ok = false
		errString += fmt.Sprintf("tvs::conf1:%v::conf2:%v::", conf1.Tvs, conf2.Tvs)
	}

	if conf1.Dds != conf2.Dds {
		ok = false
		errString += fmt.Sprintf("dds::conf1:%v::conf2:%v::", conf1.Dds, conf2.Dds)
	}

	if conf1.Pts != conf2.Pts {
		ok = false
		errString += fmt.Sprintf("pts::conf1:%v::conf2:%v::", conf1.Pts, conf2.Pts)
	}

	if !ok {
		err = errors.New(errString)
	}

	return ok, err
}

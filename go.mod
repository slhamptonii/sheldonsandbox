module sheldonsandbox

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/sessions v1.2.0
	github.com/robfig/cron/v3 v3.0.1
	gopkg.in/go-playground/assert.v1 v1.2.1
	gopkg.in/yaml.v2 v2.2.8
)

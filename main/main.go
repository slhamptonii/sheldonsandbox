package main

import (
	"log"
	"sheldonsandbox/app"
)

func main() {
	a := app.App{}

	if ok := a.Initialize(); !ok {
		log.Fatal("could not initialize the application")
	}

	a.Run()
}

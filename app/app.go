package app

import (
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"sheldonsandbox/controller"
)

type App struct {
	Router       *mux.Router
	templates    *template.Template
	functions    template.FuncMap
	errors       []string
	clientServer string
}

type initializer func(*App) bool

func (a *App) Initialize() bool {
	a.Router = mux.NewRouter()

	if ok := validateApplication(
		[]func() bool{
			controller.LoadFunctions,
			controller.LoadTemplates,
			controller.LoadSession,
			controller.LoadRequesters,
		}); !ok || !initializeRoutes(a) {
		log.Fatal("could not initialize application")
	}

	return true
}

func (a *App) Run() {
	if err := http.ListenAndServe(":80", a.Router); err != nil {
		log.Fatal(err)
	}
}

var initializeRoutes initializer = func(a *App) bool {
	a.Router.Use(controller.Sessions)
	a.Router.Methods("Get").Path("/health").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Healthy as an ox!"))
	})

	//force root to home page. may change if users come into play
	a.Router.Methods("Get").Path("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/home", http.StatusSeeOther)
	})

	//directs to static resources
	cssHandler := http.FileServer(http.Dir("./css/"))
	imagesHandler := http.FileServer(http.Dir("./img/"))
	jsHandler := http.FileServer(http.Dir("./scripts/"))

	a.Router.Methods("Get").PathPrefix("/css/").Handler(http.StripPrefix("/css/", cssHandler))
	a.Router.Methods("Get").PathPrefix("/img/").Handler(http.StripPrefix("/img/", imagesHandler))
	a.Router.Methods("Get").PathPrefix("/scripts/").Handler(http.StripPrefix("/scripts/", jsHandler))

	//direct to pages
	a.Router.Methods("Get").Path("/home").HandlerFunc(controller.Home)
	a.Router.Methods("Get").Path("/like").HandlerFunc(controller.Like)
	a.Router.Methods("Get").Path("/!like").HandlerFunc(controller.NotLike)
	a.Router.Methods("Get").Path("/imgs").HandlerFunc(controller.Imgs)
	a.Router.Methods("Get").Path("/projs").HandlerFunc(controller.Projs)
	a.Router.Methods("Get").Path("/ppl").HandlerFunc(controller.Ppl)
	a.Router.Methods("Get").Path("/about").HandlerFunc(controller.About)

	a.Router.Methods("Post").Path("/ratePlayers").HandlerFunc(controller.RatePlayers)
	return true
}

func validateApplication(fs []func() bool) bool {
	for _, f := range fs {
		if !f() {
			log.Fatal(fmt.Sprintf("could not validate %Tv", f))
			return false
		}
	}
	return true
}
